[Appearance]
ColorScheme=Dracula
Font=Hack,13,-1,7,50,0,0,0,0,0
LineSpacing=1

[Cursor Options]
CursorShape=2

[General]
Name=Arcigo
Parent=FALLBACK/
TerminalMargin=2

[Interaction Options]
UnderlineFilesEnabled=true

[Terminal Features]
BlinkingCursorEnabled=true