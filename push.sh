#!/bin/bash

# Author: Henry
# Date: 22/10/22
# Description: To Add and Push files to git


# Check if git initialized or not
function _check_git_init(){
    if [[ ! -d .git ]]; then
       echo -e "\n==> Git is not initialized !\n"
       exit 1
    fi
}

_check_git_init

# Adding files
echo -e "\n==> Adding files..."
git add . 2>/dev/null

# Check if any error occured or not
if [[ $? == 0 ]]; then
    echo -e "\n==> Files added to Git !"
else
    echo -e "\n==> Failed to add files to Git !\n"
    exit 1
fi

echo -e "\n==> Current Git Status:\n"
git status
read -p "==> Press Enter to Commit changes or hit [Ctrl+c] to terminate..."

# Default commit message

if [[ $1 == "" ]]; then
    echo -e "\n==> Commiting Changes 'Updated'"
    git commit -m "Updated"
    echo -e "\n==> Changes Commited! [Updated]\n"
else
    git commit -m "$*"
    echo -e "\n==> Changes Commited! [$*]\n"
fi

read -p "==> Press Enter to Push changes or hit [Ctrl+c] to terminate..."

echo -e "\n==> Pushing changes to remote repository..."
git push 2>/dev/null

# Check if any error occured or not
if [[ $? == 0 ]]; then
    echo -e "\n==> Successfully pushed changes to remote repository !\n"
else
    echo -e "\n==> Failed to push changes to remote repository !\n"
    exit 1
fi
